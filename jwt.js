const jwt = require('jsonwebtoken');
const jwtKey = 'jsonwebtoken_secret_key';
const jwtExpirySeconds = 3600;

module.exports = (userService) => {
    return {
        validateJWT(req, res, next) {
            if (req.headers.authorization === undefined) {
                res.status(401).end();
                return;
            }

            const token = req.headers.authorization.split(" ")[1];

            jwt.verify(token, jwtKey, {algorithm: "HS256"},  async (err, user) => {
                if (err) {
                    res.status(401).end();
                    return;
                }
                console.log(user);
                try {
                    let rep = await userService.dao.getByEmail(user.email);
                    req.user = rep;

                    return next();
                } catch(e) {
                    console.log(e);
                    res.status(401).end();
                }

            })
        },
        generateJWT(email) {
            return jwt.sign({email}, jwtKey, {
                algorithm: 'HS256',
                expiresIn: jwtExpirySeconds
            })
        }
    }
};
