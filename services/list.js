const ListDAO = require('../model/list');

module.exports = class ListService {
    constructor(db) {
        this.dao = new ListDAO(db);
    }

    isValid(list) {
        return !(!list.shop || list.shop.trim() === '');
    }

    isValidForUpdate(list) {
        if (!this.isValid(list)) return false;

        if (!list.date || list.date.trim() === '') return false;

        if (!list.id || isNaN(list.id)) return false;

        if (!Object.hasOwnProperty.bind(list)('archived')) return false;

        return true;
    }
};