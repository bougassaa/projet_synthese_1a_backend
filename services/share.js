const ShareDAO = require('../model/share');

module.exports = class ShareService {
    constructor(db) {
        this.dao = new ShareDAO(db);
    }
};