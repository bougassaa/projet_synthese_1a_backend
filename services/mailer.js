const nodemailer = require('nodemailer');

module.exports = class MailerService {
    constructor() {
        let mail = 'nodejstp042@gmail.com';
        this.mail = mail;
        this.transporter = nodemailer.createTransport({
            service: 'gmail',
            auth: {
                user: mail,
                pass: 'NodeJS.TP042'
            }
        });
    }

    sendEmail(mail, subject, body) {
        return new Promise((resolve, reject) => {
            let mailOptions = {
                from: "Confirmation de votre compte",
                to: mail,
                subject: subject,
                html: body
            };

            this.transporter.sendMail(mailOptions, (error, info) => {
                if (error) {
                    console.log(error);
                    reject(error);
                } else {
                    resolve('Email sent: ' + info.response);
                }
            });
        });

    }
};