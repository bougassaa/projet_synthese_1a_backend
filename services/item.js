const ItemDAO = require('../model/item');

module.exports = class ItemService {
    constructor(db) {
        this.dao = new ItemDAO(db);
    }
    
    isValid(item) {
        if (!item.label || item.label.trim() === '') return false;

        if (!item.quantity || isNaN(item.quantity) || item.quantity <= 0) return false;

        if (!item.list || isNaN(item.list)) return false;

        return true;
    }

    isValidForUpdate(item) {
        if (!this.isValid(item)) return false;

        if (!item.id || isNaN(item.id)) return false;

        if (!Object.hasOwnProperty.bind(item)('checked')) return false;

        return true;
    }
};