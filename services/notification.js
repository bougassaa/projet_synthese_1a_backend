const NotificationDAO = require('../model/notification');

module.exports = class NotificationService {
    constructor(db) {
        this.dao = new NotificationDAO(db);
    }
};