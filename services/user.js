const UserDAO = require('../model/user');
const User = require('../business/user');
const bcrypt = require('bcrypt');

module.exports = class UserService {
    constructor(db) {
        this.dao = new UserDAO(db);
    }

    async insert(name, email, password, active = false, admin = false) {
        const user = await this.dao.getByEmail(email.trim());
        if(user !== undefined) {
            return false;
        }
        return this.dao.insert(new User(null, name, email, this.hashPassword(password), active, admin));
    }

    async updatePassword(id, newPassword) {
        return this.dao.updatePassword(id, this.hashPassword(newPassword));
    }

    async validatePassword(email, password) {
        const user = await this.dao.getByEmail(email.trim());
        if(user === undefined) {
            return 0;
        }
        if(!user.active) {
            return 1;
        }
        if(user.blocked) {
            return 2;
        }

        return this.comparePassword(password, user.password);
    }

    async generateActiveAccount(user) {
        let time = Math.floor(Date.now() / 1000) + (24 * 60 * 60); // now + 24 h
        let rep = await this.dao.insertPermanentLink(user, time);

        return rep.insertId;
    }

    async generateLinkChangePassword(user) {
        let time = Math.floor(Date.now() / 1000) + (30 * 60); // now + 30 min
        let rep = await this.dao.insertPermanentLink(user, time);

        return rep.insertId;
    }

    async checkActiveAccount(id) {
        let rep = await this.dao.getPermanentLink(id);

        if(rep === undefined || Math.floor(Date.now() / 1000) > rep.time) {
            return false;
        }

        let user = rep.user;

        await this.dao.deletePermanentLink(id);
        await this.dao.activeAccount(user);
        let userData = await this.dao.getById(user);

        return userData.email;
    }

    async checkPasswordLink(id) {
        let rep = await this.dao.getPermanentLink(id);

        return !(rep === undefined || Math.floor(Date.now() / 1000) > rep.time);
    }

    comparePassword(password, hash) {
        return bcrypt.compareSync(password, hash)
    }

    hashPassword(password) {
        return bcrypt.hashSync(password, 10)  // 10 : cost factor -> + élevé = hash + sûr
    }
};