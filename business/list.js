module.exports = class List {
    constructor(id, shop, date, archived, user) {
        this.id = id;
        this.shop = shop;
        this.date = date;
        this.archived = archived;
        this.user = user;
    }
};