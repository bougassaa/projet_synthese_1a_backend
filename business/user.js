module.exports = class User {
    constructor(id, name, email, password, active = false, admin = false, subscriber = false) {
        this.id = id;
        this.name = name;
        this.email = email;
        this.password = password;
        this.active = active;
        this.admin = admin;
        this.subscriber = subscriber;
    }
};