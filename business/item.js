module.exports = class Item {
    constructor(id, label, quantity, checked, list) {
        this.id = id;
        this.label = label;
        this.quantity = quantity;
        this.checked = checked;
        this.list = list;
    }
};