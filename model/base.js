module.exports = class BaseDAO {
    constructor(db, table) {
        this.db = db;
        this.table = table;
    }

    getById(id, table = this.table) {
        return new Promise((resolve, reject) => {
            this.query(`SELECT * FROM ${table} WHERE id = ? LIMIT 1`, [id])
                .then(row => resolve(row[0]))
                .catch(err => reject(err));
        })
    }

    delete(id, table = this.table) {
        return this.query(`DELETE FROM ${table} WHERE id = ?`, [id]);
    }

    getRole(list, user) {
        return this.query("SELECT editable FROM share WHERE list = ? AND user = ?", [list, user]);
    }

    query(sql, args){
        return new Promise((resolve, reject) => {
            this.db.query(sql, args, (err, rows) => {
                if (err){
                    return reject(err);
                } else {
                    resolve(rows);
                }
            } );
        } );
    }
};