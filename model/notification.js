const BaseDAO = require('./base');

module.exports = class NotificationDAO extends BaseDAO {
    constructor(db) {
        super(db, "notification");
    }

    insert(title, message, user) {
        return this.query("INSERT INTO notification (title, message, user) VALUES (?, ?, ?)", [title, message, user]);
    }

    /**
     * @param title
     * @param user
     *
     * check if the notification for this user is already existing and unread
     */
    checkNotif_1(title, user) {
        return this.query("SELECT 1 FROM notification WHERE title = ? AND user = ? AND seen = FALSE", [title, user]);
    }

    getNotifOfUser(user) {
        return this.query("SELECT * FROM notification WHERE user = ? ORDER BY id DESC", [user]);
    }

    updateSeenValue(id) {
        return this.query("UPDATE notification SET seen = TRUE WHERE id = ?", [id])
    }

    getUnreadReadNotif(user) {
        return this.query("SELECT * FROM notification WHERE user = ? AND seen = FALSE ORDER BY id DESC", [user]);
    }
};