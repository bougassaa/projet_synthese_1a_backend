const BaseDAO = require('./base');

module.exports = class ShareDAO extends BaseDAO {
    constructor(db) {
        super(db, "share");
    }

    insert(list, user, editable) {
        return this.query("INSERT INTO share (list, user, editable) VALUES (?, ?, ?)", [list, user, editable]);
    }

    delete(list, user) {
        return this.query("DELETE FROM share WHERE list = ? AND user = ?", [list, user]);
    }

    getListSharedFromUser(id) {
        return this.query("SELECT list.*, count(list) as 'noOfShare' FROM share JOIN list ON list.id = share.list WHERE list.user = ? GROUP BY list", [id]);
    }

    getListSharedForOthers(id) {
        return this.query("SELECT list.*, editable, user.name FROM share JOIN list ON list.id = share.list JOIN user ON user.id = list.user WHERE share.user = ?", [id]);
    }
};