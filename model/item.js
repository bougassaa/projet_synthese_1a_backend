const BaseDAO = require('./base');

module.exports = class ItemDAO extends BaseDAO {
    constructor(db) {
        super(db, "item");
    }

    getAll(list) {
        return this.query("SELECT * FROM item WHERE list = ? ORDER BY id DESC", [list]);
    }

    /**
     * @param {Item} item
     */
    insert(item) {
        return new Promise((resolve, reject) => {
            this.db.query("INSERT INTO item (label, quantity, checked, list) VALUES (?, ?, ?, ?)", [item.label, item.quantity, item.checked, item.list], (err, results) => {
                if (err) {
                    reject(err.message);
                } else {
                    resolve(results.insertId);
                }
            });
        });
    }

    update(item) {
        return new Promise((resolve, reject) => {
            this.db.query("UPDATE item SET label = ?, quantity = ?, checked = ?, list = ? WHERE id = ?", [item.label, item.quantity, item.checked, item.list, item.id], (err, results) => {
                if (err) {
                    reject(err.message);
                } else {
                    resolve(results.changedRows);
                }
            });
        });
    }
};