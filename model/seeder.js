const Item = require('../business/item');
const List = require('../business/list');

/**
 * @param {UserService} userService
 * @param {ListService} listService
 * @param {ItemService} itemService
 * @returns {Promise<string>}
 */

module.exports = (userService, listService, itemService) => {
    return new Promise(async (resolve, reject) => {
        try {
            await userService.dao.query(`CREATE TABLE user (id INT AUTO_INCREMENT PRIMARY KEY, name VARCHAR(50), email VARCHAR(100), password VARCHAR(200), active BOOLEAN, admin BOOLEAN, blocked BOOLEAN DEFAULT FALSE, subscriber BOOLEAN DEFAULT FALSE)`);
            await listService.dao.query(`CREATE TABLE list (id INT AUTO_INCREMENT PRIMARY KEY, shop VARCHAR(50), date DATE, archived BOOLEAN, user INT)`);
            await itemService.dao.query(`CREATE TABLE item (id INT AUTO_INCREMENT PRIMARY KEY, label VARCHAR(50), quantity INT, checked BOOLEAN, list INT)`);
            await itemService.dao.query(`CREATE TABLE share (list INT NOT NULL , user INT NOT NULL , editable BOOLEAN, PRIMARY KEY(list, user))`);
            await userService.dao.query(`CREATE TABLE payment (id INT AUTO_INCREMENT PRIMARY KEY, cardowner VARCHAR(100), cardnumber VARCHAR(16), cardsecurity VARCHAR(3), carddate VARCHAR(5), ammount DECIMAL(6,2), user INT)`);
            await userService.dao.query(`CREATE TABLE permanent_link (id INT AUTO_INCREMENT PRIMARY KEY , user INT NOT NULL, time LONG NOT NULL)`);
            await userService.dao.query(`CREATE TABLE notification (id INT AUTO_INCREMENT PRIMARY KEY , title VARCHAR(100), message VARCHAR(500), user INT NOT NULL, seen BOOLEAN DEFAULT FALSE)`);
        } catch (e) {
            if(e.code === "ER_TABLE_EXISTS_ERROR") {
                resolve();
            } else {
                reject(e.code);
            }
            return;
        }

        let userId = await userService.insert("Doe Johnson", "doe@gmail.com", "pass", true, true);
        let listId = await listService.dao.insert(new List(null, "Liste 1", new Date(+(new Date()) - Math.floor(Math.random() * 10000000000)), false, userId));

        for (let i = 1; i < 5; i++) {
            await itemService.dao.insert(new Item(null, `Item ${i}`, i, false, listId));
        }
    })
};