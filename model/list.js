const BaseDAO = require('./base');

module.exports = class ListDAO extends BaseDAO {
    constructor(db) {
        super(db, "list");
    }

    getAll(user) {
        return this.query("SELECT * FROM list WHERE archived = 0 AND user = ? ORDER BY id DESC", [user.id]);
    }

    getArchived(user) {
        return this.query("SELECT * FROM list WHERE archived = 1 AND user = ? ORDER BY id DESC", [user.id]);
    }

    getCurrentList(user) {
        return this.query("SELECT * FROM list WHERE archived <> 1 AND user = ? ORDER BY id DESC", [user.id]);
    }

    /**
     * @param {List} list
     */
    insert(list) {
        return new Promise((resolve, reject) => {
            this.db.query("INSERT INTO list (shop, date, archived, user) VALUES (?, ?, ?, ?)", [list.shop, list.date, list.archived, list.user], (err, results) => {
                if (err) {
                    reject(err.message);
                } else {
                    resolve(results.insertId);
                }
            });
        });
    }

    update(list) {
        return new Promise((resolve, reject) => {
            this.db.query("UPDATE list SET shop = ?, date = ?, archived = ? WHERE id = ?", [list.shop, list.date, list.archived, list.id], (err, results) => {
                if (err) {
                    reject(err.message);
                } else {
                    resolve(results.changedRows);
                }
            });
        });
    }

    getOverdueList(user) {
        return this.query("SELECT * FROM list WHERE date < DATE_SUB(now(), INTERVAL 7 DAY) AND archived = 0 AND user = ?", [user])
    }
};