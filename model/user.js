const BaseDAO = require('./base');

module.exports = class UserDAO extends BaseDAO {
    constructor(db) {
        super(db, "user");
    }

    /**
     * @param {User} user
     */
    insert(user) {
        return new Promise((resolve, reject) => {
            this.db.query("INSERT INTO user (name, email, password, active, admin) VALUES (?, ?, ?, ?, ?)", [user.name, user.email, user.password, user.active, user.admin], (err, results) => {
                if (err) {
                    reject(err.message);
                } else {
                    resolve(results.insertId);
                }
            });
        });
    }

    /**
     * @param {User} user
     */
    update(user) {
        return this.query("UPDATE user SET name = ?, email = ?, admin = ?, blocked = ?, subscriber = ? WHERE id = ?", [user.name, user.email, user.admin, user.blocked, user.subscriber, user.id]);
    }

    updatePassword(id, pwd) {
        return this.query("UPDATE user SET password = ? WHERE id = ?", [pwd, id]);
    }

    getByEmail(email) {
        return new Promise((resolve, reject) => {
            this.query("SELECT * FROM user WHERE email = ?", [email])
                .then(row => resolve(row[0]))
                .catch(err => reject(err));
        })
    }

    getByTyping(typing, currentUser) {
        return this.query("SELECT * FROM user WHERE (name LIKE concat('%', ?, '%') OR email LIKE concat('%', ?, '%')) AND id <> ? AND active = 1", [typing, typing, currentUser]);
    }

    activeAccount(user) {
        return this.query("UPDATE user SET active = 1 WHERE id = ?", [user]);
    }

    getUserFromPermanentLink(id) {
        return new Promise((resolve, reject) => {
            this.query("SELECT user.* FROM permanent_link JOIN user ON user.id = permanent_link.user WHERE permanent_link.id = ?", [id])
                .then(row => resolve(row[0]))
                .catch(err => reject(err));
        })
    }

    deletePermanentLink(id) {
        return this.query("DELETE FROM permanent_link WHERE id = ?", [id]);
    }

    getPermanentLink(id) {
        return new Promise((resolve, reject) => {
            this.query("SELECT * FROM permanent_link WHERE id = ?", [id])
                .then(row => resolve(row[0]))
                .catch(err => reject(err));
        })
    }

    insertPermanentLink(user, time) {
        return this.query("INSERT INTO permanent_link (user, time) VALUES (?, ?)", [user, time]);
    }

    addPayment(cardowner, cardnumber, cardsecurity, carddate, ammount, user) {
        return this.query(
            "INSERT INTO payment (cardowner, cardnumber, cardsecurity, carddate, ammount, user) VALUES (?, ?, ?, ?, ?, ?)",
            [cardowner, cardnumber, cardsecurity, carddate, ammount, user]
        );
    }

    subscribers() {
        return this.query("SELECT * FROM user WHERE subscriber = 1");
    }
};