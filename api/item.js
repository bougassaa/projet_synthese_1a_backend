const Item = require('../business/item');
const NotificationDAO = require('../model/notification');

/**
 * @param {express} app
 * @param {ItemService} svc
 * @param {validateJWT} jwt
 */
module.exports = (app, svc, jwt) => {
    app.get("/item/list/:list", jwt.validateJWT, async (req, res) => {
        try {
            if(await roleForList(req.user.id, req.params.list) === 0){
                return res.status(403).end()
            }

            res.json(await svc.dao.getAll(req.params.list));
        } catch (e) {
            console.error(e);
            res.status(500).end();
        }
    });

    app.get("/item/:id", jwt.validateJWT, async (req, res) => {
        try {
            let item = await svc.dao.getById(req.params.id);

            if(await roleForList(req.user.id, item.list) === 0){
                return res.status(403).end()
            }

            res.json(item);
        } catch (e) {
            console.error(e);
            res.status(500).end();
        }
    });

    app.post("/item", jwt.validateJWT, async (req, res) => {
        const item = req.body;

        if(!svc.isValid(item)) {
            return res.status(400).end();
        }

        if(await roleForList(req.user.id, item.list) !== 2){
            return res.status(403).end()
        }

        svc.dao.insert(new Item(null, item.label, item.quantity, 0, item.list))
            .then(rep => res.json(rep))
            .catch(e => {
                console.error(e);
                res.status(500).end();
            });

        await checkForNotifListOwner(req.user.id, item.list)
    });

    app.delete("/item/:id", jwt.validateJWT, async (req, res) => {
        let item = await svc.dao.getById(req.params.id);

        if(await roleForList(req.user.id, item.list) !== 2){
            return res.status(403).end()
        }

        svc.dao.delete(req.params.id)
            .then(resp => {
                res.write(`Affected rows ${resp.affectedRows}`);
                res.status(200).end()
            })
            .catch(e => {
                console.error(e);
                res.status(500).end();
            });

        await checkForNotifListOwner(req.user.id, item.list)
    });

    app.put("/item", jwt.validateJWT, async (req, res) => {
        const item = req.body;

        if(!svc.isValidForUpdate(item)) {
            return res.status(400).end();
        }

        if(await roleForList(req.user.id, item.list) !== 2){
            return res.status(403).end()
        }

        svc.dao.update(item)
            .then(resp => {
                res.write(`Affected rows ${resp}`);
                res.status(200).end()
            })
            .catch(e => {
                console.error(e);
                res.status(500).end();
            });

        await checkForNotifListOwner(req.user.id, item.list)
    });

    async function roleForList(user, listId) {
        let list = await svc.dao.getById(listId, "list");
        if(user === list.user) {
            return 2;
        }

        let rep = await svc.dao.getRole(listId, user);

        if(rep[0].editable === 0) {
            return 1;
        }

        if(rep[0].editable === 1) {
            return 2;
        }

        return 0;
    }

    async function checkForNotifListOwner(user, listId) {
        let list = await svc.dao.getById(listId, "list");

        if(user !== list.user) {
            let currentUser = await svc.dao.getById(user, "user")
            let notificationDAO = new NotificationDAO(svc.dao.db)

            let title = `Modification sur la liste N°${listId}`

            let rep = await notificationDAO.checkNotif_1(title, list.user)
            if(rep.length === 0){
                notificationDAO.insert(title, `Votre liste (${list.shop}) a été modifié par ${currentUser.name}.`, list.user)
            }
        }
    }
};