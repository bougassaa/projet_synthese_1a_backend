const User = require('../business/user');
const NotificationDAO = require('../model/notification');
const ListDAO = require('../model/list');

/**
 * @param {express} app
 * @param {UserService} svc
 * @param {validateJWT} jwt
 * @param {MailerService} Mailer
 */
module.exports = (app, svc, jwt, Mailer) => {
    app.post("/user/authenticate", async (req, res) => {
        const { email, password } = req.body;

        if ((email === undefined) || (password === undefined)) {
            console.log(req.body);
            res.status(400).end();
            return
        }
        let rep = await svc.validatePassword(email, password);

        if(rep === 0) { // si aucun utilisateur est trouvé
            res.status(401).end();
        } else if(rep === 1) { // si le compte n'es pas activé, pas l'autorisation d'accéder au compte
            res.status(403).end();
        } else if(rep === 2) { // si le compte a été bloqué
            res.status(402).end();
        } else if(rep === true) {
            let user = await svc.dao.getByEmail(email)
            await checkOverdueList(user.id)

            res.json({'token': jwt.generateJWT(email)});
        } else {
            res.status(401).end();
        }
    });

    app.post("/user/create", async (req, res) => {
        const { email, name, password, baselink } = req.body;

        if ((email === undefined) || (password === undefined) || (name === undefined)) {
            res.status(400).end();
            return
        }

        const rep = await svc.insert(name, email, password);

        if(rep === false) {
            res.status(401).end();
            return
        }

        sendActiveAccount(rep, email, baselink)
            .then(_ => res.status(200).end())
            .catch(_ => res.status(400).end());

        let notificationDAO = new NotificationDAO(svc.dao.db)
        notificationDAO.insert(`Bienvenue ${name}`, `Bienvenue sur le site ${name}, quand vous aurez du temps abonnez vous pour profiter pleinement du site !`, rep)
    });

    app.post("/user/typing", jwt.validateJWT, async (req, res) => {
        const { typing } = req.body;
        try {
            let users = await svc.dao.getByTyping(typing, req.user.id);
            res.json(users);
        } catch (e) {
            console.error(e);
            res.status(500).end();
        }
    });

    app.get("/user/actived/:id", async (req, res) => {
        let email = await svc.checkActiveAccount(req.params.id);

        if(email) {
            res.json({'token': jwt.generateJWT(email)});
        } else {
            res.status(403).end()
        }
    });

    app.post("/user/subscribe", jwt.validateJWT, async (req, res) => {
        const { cardowner, cardnumber, cardsecurity, carddate } = req.body;

        svc.dao.addPayment(cardowner, cardnumber, cardsecurity, carddate, 13.99, req.user.id).then(_ => {
                svc.dao.getById(req.user.id).then(async user => {
                    user.subscriber = true;
                    svc.dao.update(user);
                    await sendPaymentConfirmation(user.email);
                    res.json({success: true});
                })
            })
            .catch(_ => res.status(404).end())
    });

    app.get("/user/subscriber", jwt.validateJWT, async (req, res) => {
        try {
            let subscribers = await svc.dao.subscribers();
            res.json(subscribers);
        } catch (e) {
            console.error(e);
            res.status(500).end();
        }
    });

    app.get("/user/checkPasswordLink/:id", async (req, res) => {
        if(await svc.checkPasswordLink(req.params.id)) {
            res.status(200).end()
        } else {
            res.status(404).end()
        }
    });

    app.post("/user/newActivationId", async (req, res) => {
        const { id, baselink } = req.body;

        let user = await svc.dao.getUserFromPermanentLink(id);

        if(user !== undefined && !user.active) {
            await sendActiveAccount(user.id, user.email, baselink);
            res.json({'email': user.email});
        } else {
            res.status(404).end();
        }
    });

    app.post("/user/newActivationEmail", async (req, res) => {
        const { email, baselink } = req.body;

        let user = await svc.dao.getByEmail(email);

        if(user !== undefined && !user.active) {
            await sendActiveAccount(user.id, user.email, baselink);
            res.json({'email': user.email});
        } else {
            res.status(404).end();
        }
    });

    app.post("/user/linkChangePassword", async (req, res) => {
        const { email, baselink } = req.body;

        let user = await svc.dao.getByEmail(email);

        if(user !== undefined) {
            await sendLinkChangePassword(user.id, user.email, baselink);
            res.json({'email': user.email});
        } else {
            res.status(404).end();
        }
    });

    app.put("/user/changePasswordFromLink", async (req, res) => {
        const { id, password } = req.body;

        let user = await svc.dao.getUserFromPermanentLink(id);

        if(user !== undefined) {
            await svc.dao.deletePermanentLink(id);
            await svc.updatePassword(user.id, password);

            let token = null;

            if(user.active) {
                token = jwt.generateJWT(user.email);
            }

            res.json({'token': token});
        } else {
            res.status(404).end();
        }
    });

    app.put("/user/changeEmail", jwt.validateJWT, async (req, res) => {
        const { user, email } = req.body;

        if(await isAdmin(req.user.id)) {
            let changeUser = await svc.dao.getById(user);
            changeUser.email = email;

            await svc.dao.update(changeUser);
            res.status(200).end();
        } else {
            return res.status(401).end();
        }
    });

    app.get("/user/checkIfSubscriber", jwt.validateJWT, async (req, res) => {
        try {
            let users = await svc.dao.getById(req.user.id);
            res.json({'subscriber' : users.subscriber});
        } catch (e) {
            console.error(e);
            res.status(500).end();
        }
    });

    async function sendActiveAccount(user, email, baseLink) {
        let id = await svc.generateActiveAccount(user);

        return Mailer.sendEmail(
            email,
            "Validation de votre adresse e-mail",
            `Bonjour,<br>Veuillez confirmer votre adresse e-mail en cliquant <a href='${baseLink}?active=${id}'>ici</a>.`
        );
    }

    async function sendLinkChangePassword(user, email, baseLink) {
        let id = await svc.generateLinkChangePassword(user);

        return Mailer.sendEmail(
            email,
            "Changement de votre mot de passe",
            `Bonjour,<br>Vous avez demander a changer votre mot de passe cliquez <a href='${baseLink}?newPwd=${id}'>ici</a> pour acceder au formulaire.`
        );
    }

    async function sendPaymentConfirmation(email) {
        return Mailer.sendEmail(
            email,
            "Confirmation de paiement",
            `Bonjour,<br>
            Vous venez de souscrire à un abonnement sur notre site, le paiement a été effectué par carte bancaire, pour un montant de <strong>13,99€ / mois</strong> sans engagement.<br>
            Cordialement.`
        );
    }

    app.get("/user", jwt.validateJWT, async (req, res) => {
        try {
            let users = await svc.dao.getById(req.user.id);
            users.password = "fake.Pwd";

            res.json(users);
        } catch (e) {
            console.error(e);
            res.status(500).end();
        }
    });

    app.get("/user/:id", jwt.validateJWT, async (req, res) => {
        try {
            let users = await svc.dao.getById(req.params.id);

            res.json(users);
        } catch (e) {
            console.error(e);
            res.status(500).end();
        }
    });

    app.put("/user", jwt.validateJWT, async (req, res) => {
        const user = req.body;

        if(user.id !== req.user.id) {
            return res.status(403).end();
        }

        if(user.password !== "fake.Pwd") {
            await svc.updatePassword(user.id, user.password);
        }

        await svc.dao.update(user);

        res.json({'token': jwt.generateJWT(user.email)});
    });

    app.put("/user/admin", jwt.validateJWT, async (req, res) => {
        if(await isAdmin(req.user.id)) {
            const user = req.body;

            await svc.dao.update(user);
            res.status(200).end();
        } else {
            return res.status(401).end();
        }
    });

    async function isAdmin(user) {
        let currentUser = await svc.dao.getById(user);
        return currentUser.admin;
    }

    async function checkOverdueList(user) {
        let notificationDAO = new NotificationDAO(svc.dao.db)
        let listDAO = new ListDAO(svc.dao.db)

        let overdueList = await listDAO.getOverdueList(user)

        for (let list of overdueList) {
            let title = `Liste N°${list.id} périmée`

            let rep = await notificationDAO.checkNotif_1(title, user)

            if(rep.length === 0){
                notificationDAO.insert(title, `Votre liste (${list.shop}) est périmée (elle dépasse 7 jours) vous devez soit la supprimer ou l'archiver.`, user)
            }
        }
    }
};