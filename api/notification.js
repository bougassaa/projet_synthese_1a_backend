module.exports = (app, svc, jwt) => {
    app.post("/notification", jwt.validateJWT, async (req, res) => {
        const {title, message, user} = req.body;

        svc.dao.insert(title, message, user)
            .then(res.status(200).end())
            .catch(e => {
                console.log(e.code)
                res.status(404).end()
            });
    });

    app.get("/notification", jwt.validateJWT, async (req, res) => {
        try {
            res.json(await svc.dao.getNotifOfUser(req.user.id));
        } catch (e) {
            console.error(e);
            res.status(500).end();
        }
    });

    app.get("/notification/unread", jwt.validateJWT, async (req, res) => {
        try {
            res.json(await svc.dao.getUnreadReadNotif(req.user.id));
        } catch (e) {
            console.error(e);
            res.status(500).end();
        }
    });

    /**
     * update seen value in db false -> true
     */
    app.put("/notification/:id", jwt.validateJWT, async (req, res) => {
        svc.dao.updateSeenValue(req.params.id)
            .then(res.status(200).end())
            .catch(e => {
                console.log(e.code)
                res.status(404).end()
            });
    });
};