const List = require('../business/list');

/**
 * @param {express} app
 * @param {ListService} svc
 * @param {validateJWT} jwt
 */
module.exports = (app, svc, jwt) => {
    app.get("/list", jwt.validateJWT, async (req, res) => {
        try {
            res.json(await svc.dao.getAll(req.user));
        } catch (e) {
            console.error(e);
            res.status(500).end();
        }
    });

    app.get("/list/archived", jwt.validateJWT, async (req, res) => {
        try {
            res.json(await svc.dao.getArchived(req.user));
        } catch (e) {
            console.error(e);
            res.status(500).end();
        }
    });

    app.get("/list/noOfCurrentList", jwt.validateJWT, async (req, res) => {
        try {
            let rep = await svc.dao.getCurrentList(req.user);
            res.json({'noOfCurrentList' : rep.length});
        } catch (e) {
            console.error(e);
            res.status(500).end();
        }
    });

    app.get("/list/:id", jwt.validateJWT, async (req, res) => {
        try {
            let list = await svc.dao.getById(req.params.id);

            if (list === undefined) {
                return res.status(404).end()
            }

            if(list.user !== req.user.id){
                return res.status(403).end()
            }
            res.json(list);
        } catch (e) {
            console.error(e);
            res.status(500).end();
        }
    });

    app.post("/list", jwt.validateJWT, (req, res) => {
        const list = req.body;

        if(!svc.isValid(list)) {
            return res.status(400).end();
        }

        svc.dao.insert(new List(null, list.shop, new Date(), 0, req.user.id))
            .then(rep => res.json(rep))
            .catch(e => {
                console.error(e);
                res.status(500).end();
            });
    });

    app.delete("/list/:id", jwt.validateJWT, async (req, res) => {
        let list = await svc.dao.getById(req.params.id);

        if (list === undefined) {
            return res.status(404).end()
        }

        if(list.user !== req.user.id){
            return res.status(403).end()
        }

        svc.dao.delete(req.params.id)
            .then(resp => {
                res.write(`Affected rows ${resp.affectedRows}`);
                res.status(200).end()
            })
            .catch(e => {
                console.error(e);
                res.status(500).end();
            });
    });

    app.put("/list", jwt.validateJWT, async (req, res) => {
        const list = req.body;

        if(!svc.isValidForUpdate(list)) {
            return res.status(400).end();
        }

        let prevList = await svc.dao.getById(list.id);

        if(prevList.user !== req.user.id || list.user !== req.user.id){
            return res.status(403).end()
        }

        svc.dao.update(list)
            .then(resp => {
                res.write(`Affected rows ${resp}`);
                res.status(200).end()
            })
            .catch(e => {
                console.error(e);
                res.status(500).end();
            });
    });
};