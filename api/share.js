/**
 * @param {express} app
 * @param {ShareService} svc
 * @param {validateJWT} jwt
 */
module.exports = (app, svc, jwt) => {
    app.post("/share", jwt.validateJWT, async (req, res) => {
        const {list, user, edit} = req.body;

        let listData = await svc.dao.getById(list, "list");

        if(listData.user !== req.user.id){
            return res.status(403).end();
        }

        if(user === req.user.id){
            return res.status(404).end();
        }

        await svc.dao.delete(list, user);

        svc.dao.insert(list, user, edit)
            .catch(e => console.log(e.code));

        res.status(200).end();
    });

    app.get("/share/fromuser", jwt.validateJWT, async (req, res) => {
        try {
            res.json(await svc.dao.getListSharedFromUser(req.user.id));
        } catch (e) {
            console.error(e);
            res.status(500).end();
        }
    });

    app.get("/share/forothers", jwt.validateJWT, async (req, res) => {
        try {
            res.json(await svc.dao.getListSharedForOthers(req.user.id));
        } catch (e) {
            console.error(e);
            res.status(500).end();
        }
    });
};