const mysql = require('mysql');
const express = require('express');
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const cors = require('cors');
const morgan = require('morgan');
const nocache = require('nocache');

const app = express();
app.use(nocache());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(cors());
app.use(morgan('dev'));
app.use(cookieParser());

// https://www.npmjs.com/package/mysql
// https://dev.mysql.com/doc/refman/8.0/en/server-error-reference.html
require('dotenv').config()

const db = mysql.createPool({
    connectionLimit : 10,
    host     : process.env.DB_HOST,
    user     : process.env.DB_USER,
    password : process.env.DB_PASS,
    database : process.env.DB_NAME
});

const UserService = require('./services/user');
const userService = new UserService(db);

const ListService = require('./services/list');
const listService = new ListService(db);

const ItemService = require('./services/item');
const itemService = new ItemService(db);

const ShareService = require('./services/share');
const shareService = new ShareService(db);

const NotificationService = require('./services/notification');
const notificationService = new NotificationService(db);

const jwt = require('./jwt')(userService);
const Mailer = require('./services/mailer');

require('./api/list')(app, listService, jwt);
require('./api/item')(app, itemService, jwt);
require('./api/share')(app, shareService, jwt);
require('./api/user')(app, userService, jwt, new Mailer());
require('./api/notification')(app, notificationService, jwt);

require('./model/seeder')(userService, listService, itemService).then(_ => {
    app.listen(3333);
});

